<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@getIndex');

Route::get('/about', 'PagesController@getAbout');

Route::get('/contact', 'PagesController@getContact');

Route::get('/{user}/friends',[
  'uses' => 'UserController@showFriends',
  'as' => 'users.showFriends'
]);

Route::post('/searchFriends',[
  'uses' => 'UserController@searchFriends',
  'as' => 'users.searchFriends'
]);

Route::get('/{user}/profile',[
  'uses' => 'UserController@showProfile',
  'as' => 'users.showProfile'
]);

Route::get('/{user}/sendFriendRequest',[
  'uses' => 'UserController@sendFriendRequest',
  'as' => 'users.sendFriendRequest'
]);

Route::get('/{user}/deleteFriends',[
  'uses' => 'UserController@deleteFriends',
  'as' => 'users.deleteFriends'
]);

Route::get('/deletePost/{postid}',[
  'uses' => 'PostController@deletePost',
  'as' => 'posts.deletePost'
]);

Route::post('/addComment/{post}',[
  'uses' => 'PostController@addComment',
  'as' => 'posts.addComment'
]);

Auth::routes();

Route::get('/more', 'PagesController@getMore');

Route::resource('users' , 'UserController');

Route::resource('posts' , 'PostController');
