@extends('layouts.app')


@section('content')

  @if (Auth::guest())
<div class="jumbotron">
  <h1>Witaj!</h1>
  <p>Lifters.pl to strona zrzeszająca pakierów od setek lat.</p>
  <p><a class="btn btn-primary btn-lg" href="more" role="button">Dowiedz się więcej...</a></p>
</div>
@elseif(Auth::check())

@include('partials.userScreen')

@endif

@endsection
