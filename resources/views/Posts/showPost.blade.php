@extends('layouts.app')

@section('content')



        <div class="panel panel-success">
          <div class="panel-heading">
            <div class="panel-title">
                    <div class="row">
                            <div class="col-md-7">
                                    {{$post->title}}
                            </div>

                            <div class="col-md-offset-1 col-md-4">
                                   <small><a class="text-right" href="{{route('users.showProfile',$post->user_id)}}">{{$post->name .' '. $post->last_name}}</a></small>
                           </div>
                    </div>


            </div>

         </div>
          <div class="panel-body">
              {{$post->body}}

            </div>
<div class="panel-body">
          <nav class="navbar-right" style="margin:auto">
              <a href="#" class="showComments"><span class='badge'>Wyświetl komentarze</span></a>
          </nav>
 </div>

<div class="panel-footer post-comments">
        @foreach($post->comments as $comment)
        <div class="well well-sm">{{$comment->comment_body}}</div>
        @endforeach
</div>
        </div>


@endsection

@section('scripts')
<script src="{{ asset('js/myjquery.js') }}"></script>
@endsection
