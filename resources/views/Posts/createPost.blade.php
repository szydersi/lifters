@extends('layouts.app')

@section('content')
<div class="container">
  <form class="form-horizontal" action="{{ route('posts.store') }}" method="POST">
    {{ csrf_field() }}
  <div class="form-group" role="form">
    <label for="Title">Tytuł posta</label>
    <input type="text" class="form-control" id="title" name="title" placeholder="Wpisz tytuł posta">

    <hr>

    <textarea class="form-control" name="body" rows="8" cols="80" placeholder="Wpisz treść posta"></textarea>

    <hr>

    <button type="submit" name="button" class="btn btn-success btn-lg btn-block">Stwórz posta</button>
  </div>
  </form>
</div>
<br><br>




@endsection
