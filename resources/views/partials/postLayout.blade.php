        <div class="panel panel-success">
          <div class="panel-heading">
            <div class="panel-title">
                    <div class="row">
                            <div class="col-md-7">
                                    {{$post->title}}
                            </div>

                            <div class="col-md-offset-1 col-md-4">
                                   <small><a class="text-right" href="{{route('users.showProfile',$post->user_id)}}">{{$post->name .' '. $post->last_name}}</a></small>
                           </div>
                    </div>


            </div>

         </div>
          <div class="panel-body">
              {{$post->body}}



</div>
<div class="panel-body">
          <nav class="navbar-right" style="margin:auto">
                  <button type="button" class="btn btn-default btn-xs addComment" >
                          <span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Szybka Odpowiedź
                </button>
                        <a href="{{route('posts.show',$post->post_id)}}">
                                <button type="button" class="btn btn-default btn-xs">
                                        <span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Przejdź do postu
                                </button>
                        </a>
                    <a href="#">
                            <button type="button" class="btn btn-default btn-xs">
                                    <span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Poka komenty
                            </button>
                    </a>
                    @if($post->user_id==Auth::user()->id)
                      <a href="{{route('posts.deletePost', $post->post_id)}}">
                            <button type="button" class="btn btn-default btn-xs">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Usun post
                            </button>
                      </a>
                    @endif
          </nav>
 </div>
                <div class="panel-footer post-footer" >
                        <form class="form-group form-inline" action="{{ route('posts.addComment',$post->post_id) }}" method="post">
                                  {{ csrf_field() }}
                                  <label for="comment_body" class="sr-only"></label>
                          <textarea class="form-control input-sm" id="comment_body" name="comment_body" cols='70' placeholder="Wpisz komentarz..." style="height:auto";></textarea>
                         <button type="submit" name="button" class="btn btn-success btn-xs">Dodaj</button>

                  </form>

                </div>
        </div>
