<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Dodaj post</h4>
      </div>
      <div class="modal-body">

          <form class="form" action="{{ route('posts.store') }}" method="POST">
            {{ csrf_field() }}
          <div class="form-group" role="form">
            <label for="Title"></label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Wpisz tytuł posta">

            <hr>

            <textarea class="form-control" name="body" rows="8" cols="60" placeholder="Wpisz treść posta"></textarea>

          </div>
          <p class="text-right">
            <button type="submit" name="button" class="btn btn-success">Stwórz posta</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>
        </p>


          </form>

      </div>
    </div>
  </div>
</div>
<!--Koniec modala-->

<div class="container">
  <div class="row">
            <div class="col-md-2">
            <!--    <a href="#" class="thumbnail"> -->
              <img src="" alt="{{Auth::user()->name}}" style="height:180px;width:172px;">
            <!--     </a>-->  {{Auth::user()->getFullName()}}<br />
                              {{Auth::user()->city}}<br>
                              user id : {{Auth::user()->id}}

            </div>





            <div class="col-md-7" style="height:100px;">
              
                  @foreach($posts as $post)

                  @include('partials.postLayout')
                  @endforeach
            <div class="col-md-3">

            </div>
  </div>
  <div class="row">
    <div class="col-md-2 panel-body">
          <a href="" role="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary"><i class="fa fa-btn fa-plus"></i> Dodaj post</a>
    </div>
  </div>

</div>
