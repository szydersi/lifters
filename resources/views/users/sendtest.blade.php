@extends('layouts.App')

@section('content')

<table class="table table-striped">
    <thead>
        <td>Imie</td>
        <td>Nazwisko</td>
        <td>Opcje</td>
    </thead>
    <tbody>
        @foreach ($user->friends as $friend)
        <tr>
        <td>{{$friend->name}}</td>
        <td>{{$friend->last_name}}</td>
        <td></td>
        </tr>
        @endforeach
    </tbody>

</table>


@endsection
