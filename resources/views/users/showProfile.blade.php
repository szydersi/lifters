@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
            <div class="col-md-2">
            <!--    <a href="#" class="thumbnail"> -->
              <img src="..." alt="{{Auth::user()->name}}" style="height:180px;width:172px;">
            <!--     </a>-->
            {{$user->getFullName()}}<br />
              {{$user->city}}<br>
              user id : {{$user->id}}
              <bR /><br />
              @if($user->id != Auth::user()->id)
                @if(Auth::user()->friends->find($user->id) == null)
                  <a href="{{route('users.sendFriendRequest',$user)}}" role="button" class="btn btn-primary"><i class="fa fa-btn fa-plus"></i> Dodaj do znajomych</a>
                @elseif(Auth::user()->friends->find($user->id) != null)
                <span class="label label-success">Jesteście już znajomymi!</span>
                @endif
              @else

              @endif
            </div>

            <div class="col-md-7" style="height:100px;">
              <!--style="height:480px"-->
              @foreach($posts as $post)
              @include('partials.postLayout')
              @endforeach

            <div class="col-md-3">

            </div>
  </div>
  <div class="row">
    <div class="col-md-2 panel-body">

    </div>
  </div>
</div>

@endsection
