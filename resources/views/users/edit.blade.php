@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-3">
          <div class="thumbnail">
            <img src="/uploads/avatars/{{Auth::user()->avatar}}" style="width:150px; height:150px; border-radius:50%;">
            <div class="caption" style="text-align:center">
              <h3>{{Auth::user()->name}}</h3>
              <p><a href="#" class="btn btn-primary" role="button"><i class="fa fa-btn fa-share"></i> Zmień swój avatar</a>
            </div>
          </div>
        </div>

        <div class="col-md-7 col-md-offset-1">
                <center><h2>Zmień swoje dane </h2></center>
                <form class="form-horizontal" action="{{ route('users.store') }}" method="post">
                      {{ csrf_field() }}
                        <div class="form-group"role="form">
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="" value="{{$id}}">
                      <label for="name">Imię : </label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="">

                      <label for="name">Nazwisko :</label>
                      <input type="text" class="form-control" id="last_name" name="last_name" placeholder="">

                      <label for="name">Miejscowość :</label>
                      <input type="text" class="form-control" id="city" name="city" placeholder="">

                      <label for="name">Data urodzenia :</label>
                      <input type="text" class="form-control" id="birth_date" name="birth_date" placeholder="">
                      <hr>
                      <button type="submit" name="button" class="btn btn-success btn-lg btn-block">Zapisz</button>
                    </div>
                </form>

        </div>

    </div>

</div>


@endsection
