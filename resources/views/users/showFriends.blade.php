@extends('layouts.app')

@section('content')

<table class="table table-striped">
    <thead>
        <td>Imię i Nazwisko</td>
        <td></td>
    </thead>
    <tbody>
        @foreach ($user->friends as $friend)
        <tr>
        <td>{{$friend->getFullName()}}</td>
        <td>
        <a href="{{route('users.showProfile',$friend)}}" role="button" class="btn btn-md"><i title="Pokaż profil" class="fa fa-btn fa-user-circle fa-2x"></i></a>
        <a href="{{route('users.deleteFriends',$friend)}}" role="button" class="btn btn-md"><i title="Usuń znajomego" class="fa fa-btn fa-times fa-2x"></i></a>
        </td>
        </tr>
        @endforeach
    </tbody>

</table>


@endsection
