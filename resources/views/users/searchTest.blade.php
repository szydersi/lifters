@extends('layouts.app')

@section('content')
<div class="container">

<table class="table table-striped">
    <thead>
      <tr>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Miejscowość</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $user)
      <tr>
        <td>{{$user->name}}</td>
        <td>{{$user->last_name}}</td>
        <td>{{$user->city}}</td>
        <td><a class="btn btn-success" href="{{route('users.showProfile',$user->id)}}">Pokaz profil</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>

</div>
@endsection
