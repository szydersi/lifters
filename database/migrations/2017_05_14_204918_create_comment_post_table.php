<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('comment_post', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('comment_id')->unsigned();
          $table->integer('post_id')->unsigned();

          $table->foreign('comment_id')->references('id')->on('comments');
          $table->foreign('post_id')->references('id')->on('posts');

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
