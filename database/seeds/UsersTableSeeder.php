<?php

use Illuminate\Database\Seeder;
use \App\User;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i=1; $i < 4; $i++) {
        $user = new User();
        $user->name = 'User'.$i;
        $user->email = 'user'.$i.'@user.com';
        $user->password = bcrypt('user'.$i);
        $user->save();
      }

      $faker = Faker::create();
    	foreach (range(1,20) as $index) {
	        DB::table('users')->insert([
	            'name' => $faker->firstname,
	            'email' => $faker->email,
                'last_name' => $faker->lastname,
                'city' => $faker->city,
	            'password' => bcrypt('secret')
	        ]);

    }
}
}
