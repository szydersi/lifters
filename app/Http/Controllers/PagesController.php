<?php

namespace App\Http\Controllers;
use \App\User;
use DB;
use Illuminate\Http\Request;
use Auth;

class PagesController extends Controller
{

    public function getIndex(){
      if(Auth::check()){
        $user_friends = Auth::user()->friends;

            $posts = DB::table('posts')
            ->join('post_user', 'posts.id', '=', 'post_user.post_id')
            ->join('users', 'users.id', '=', 'post_user.user_id')

            ->whereIn('post_user.user_id',$user_friends->pluck('id')->all())
            ->orWhere('post_user.user_id',Auth::user()->id)
            ->orderBy('posts.created_at', 'desc')
            ->get();
      }
      return view('pages.welcome',compact('posts'));
    }

    public function getAbout(){
      return view('pages.about');
    }

    public function getContact(){     //Kontakt
      return view('pages.contact');
    }


    public function getMore(){
      return view('pages.more');
    }





  }
