<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;

use DB;
use Input;
use Auth;


class UserController extends Controller
{
    public function __construct()
    {
    $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('users.userScreen');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = User::findOrFail($request->id);
        $user->name = Input::get('name');
        $user->last_name = Input::get('last_name');
        $user->city = Input::get('city');
        $user->birth_date = Input::get('birth_date');
        $user->save();

            return redirect('/');
    }

    public function searchFriends(Request $request){
        $field = $request->field;
        if($field == ''){
            $users = DB::table('users')->get();
        }else{
        $users = DB::table('users')
            ->where('name', $field)
            ->orWhere('last_name', $field)
            ->orWhere('city', $field)
            ->orWhere('email', $field)
            ->get();
        }

        return view('users.searchTest', compact('users'));
    }


    public function showFriends(User $user){

        return view('users.showFriends',compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showProfile(User $user)
    {
      $user_friends = $user->friends;

          $posts = DB::table('posts')
          ->join('post_user', 'posts.id', '=', 'post_user.post_id')
          ->join('users', 'users.id', '=', 'post_user.user_id')
          ->whereIn('user_id',$user_friends->pluck('id')->all())
          ->orWhere('user_id',$user->id)
          ->orderBy('posts.created_at', 'desc')
          ->get();
        return view('users.showProfile',compact('user','posts'));
    }

    public function sendFriendRequest(User $user){
            Auth::user()->addFriend($user);
            return redirect(route('users.showProfile',$user));
    }

    public function deleteFriends(User $user){
     Auth::user()->removeFriend($user);
     return redirect('/');
      }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('users.edit',compact('id'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
