<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Comment;
use Auth;
use Input;
use DB;

class PostController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_friends = Auth::user()->friends;

        $posts = DB::table('posts')
        ->join('post_user', 'posts.id', '=', 'post_user.post_id')
        ->join('users', 'users.id', '=', 'post_user.user_id')
        ->whereIn('user_id',$user_friends->pluck('id')->all())
        ->orWhere('user_id',Auth::user()->id)
        ->orderBy('posts.created_at', 'desc')
        ->get();
        return view('posts.posttest',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.createPost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $post = new Post();
        $post->title = Input::get('title');
        $post->body = Input::get('body');
        $post->save();
        $user->posts()->attach($post->id);

    return redirect('/');
    }

    public function addComment($post){
        $comment = new Comment();
        $comment->comment_body = Input::get('comment_body');
        $comment->user_id = Auth::user()->id;
        $comment->save();
        $comment->posts()->attach($post);

        return redirect('/');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

        return view('posts.showPost',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function deletePost($postid){
        Auth::user()->posts()->detach($postid);
        $post = Post::find($postid);
        $comments = $post->comments;
        $post->comments()->detach($comments);
         DB::table('posts')->where('id', '=', $postid)->delete();

         $posts = DB::table('posts')
         ->join('post_user', 'posts.id', '=', 'post_user.post_id')
         ->join('users', 'users.id', '=', 'post_user.user_id')
         ->whereIn('user_id',Auth::user()->friends->pluck('id')->all())
         ->orWhere('user_id',Auth::user()->id)
         ->orderBy('posts.created_at', 'desc')
         ->get();

         return redirect()->action('PagesController@getIndex');
     }

    public function destroy($id)
    {
    //
    }
}
